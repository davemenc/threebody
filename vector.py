import math

class Vector:
        '''very simple 3D vector class'''
        def __init__(self,x,y,z):
                self.x=float(x)
                self.y=float(y)
                self.z=float(z)

        def add(self,v):
                '''adds new vector to me'''
                self.x=self.x+v.x
                self.y=self.y+v.y
                self.z=self.z+v.z

        def subtract(self,v):
                '''subtracts new vector from me'''
                self.x=self.x-v.x
                self.y=self.y-v.y
                self.z=self.z-v.z

        def setvect(self,v):
                '''sets me from a new vector'''
                self.x=v.x
                self.y=v.y
                self.z=v.z
        
        def zero(self):
                '''zero out me'''
                self.x=0.0
                self.y=0.0
                self.z=0.0
        
        def string(self):
                '''return a string with my values'''
                return "{x:G},{y:G},{z:G}".format (x=self.x,y=self.y,z=self.z)

        def local_print(self):
                '''print my values'''
                print self.string()
        
        def setvals(self,x,y,z):
                '''set my values from individual values'''
                self.x=float(x)
                self.y=float(y)
                self.z=float(z)
        
        def VectAngle(self,v):
                x1 = float(self.x)
                x2 = float(v.x)
                y1 = float(self.y)
                y2 = float(v.y)
                z1 = float(self.z)
                z2 = float(v.z)
                Theta = math.acos(((x1*x2)+(y1*y2)+(z1*z2))/(math.sqrt(x1**2+y1**2+z1**2)*math.sqrt(x2**2+y2**2+z2**2)))
                return Theta
         
        def Distance(Self,v):
                x1 = float(self.x)
                x2 = float(v.x)
                y1 = float(self.y)
                y2 = float(v.y)
                z1 = float(self.z)
                z2 = float(v.z)
                return math.sqrt((x1-x2)**2+(y1-y2)**2+(z1-z2)**2)
