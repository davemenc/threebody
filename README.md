# README #



### What is this repository for? ###

This repository is my own attempt to simulate celestial bodies in orbits. It is not the first time someone has done this nor (Lord knows) the best but it is MINE. 

I always wanted to do this but I just read "three body" (a sci fi book -- didn't like it) so I'm suddenly inspired. Strike while the fire is hot!

The medium goal is to set initial conditions and use PyGame (or some such) to display the results.

My real goal is to write a genetic algorithm that will find "interesting" configurations of bodies where "interesting" is defined by things that don't fly off into space. I suspect that there will be a range of values -- any values in that range will meet the definition. If I were a celestial mechanic I would figure this out in a straightforward way for a mathematician (I would guess using calculus) but I'm a software engineer so I will figure it out in a straightforward way (for a software engineer). 

Once I have code that always generates "interesting" configurations I might go on to rate the configurations in terms of how exciting they are. 

### How do I get set up? ###
Extremely simple as I only need Math; eventually it might pygame and a database. 

### Contribution guidelines ###

Feel free to look and even make suggestions. 

### Who do I talk to? ###

My name is Dave Menconi and my email address is first name at last name dot com.