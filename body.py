import math, vector
class Body:
        '''a body is an astronomical body with mass, position and velocity'''
        def __init__(self, VelV, PosV, Mass=2E30, Radius=7.0E8):
                self.Mass = Mass
                self.Radius = Radius
                self.PosVector = PosV
                self.VelVector = VelV
                self.AccVector = Vector(0.0,0.0,0.0)


        def distance(self,body):
                return math.sqrt((self.PosVector.x-body.PosVector.x)^2+(self.PosVector.y-body.PosVector.y)^2+(self.PosVector.z-body.PosVector.z)^2)

        def SetAcceleration(self,AccVec):
                self.AccVector = AccVec

        def AddAcceleration(self,AccVec):
                self.AccVector.add(AccVec)

        def Accelerate(self):
                self.AccVector.add(AccVector)

        def SetAccelerationZero(self):
                selfAccVector = Vector(0.0,0.0,0.0)

        def string(self):
                s = "Mass: {}\n".format(self.Mass)
                s += "Radius: {}\n".format(self.Radius)
                s += "Position: "+self.PosVector.string()+"\n"
                s += "Velocity: "+self.VelVector.string()+"\n"
                s += "Acceleration: "+self.AccVector.string()+"\n"
  
        def local_print(self):
                print self.string()


        def GravForce(self,body):
                x1 = float(self.PosVec.x)
                x2 = float(body.PosVec.x)
                y1 = float(self.PosVec.y)
                y2 = float(body.PosVec.y)
                z1 = float(self.PosVec.z)
                z2 = float(body.PosVec.z)
                m1 = float(self.Mass)
                m2 = float(body.Mass)
                G = 6.67408E-11

                rx=x1-x2
                ry=y1-y2
                rz=z1-z2
                r = Distance(self.PosVec,body.PosVec)

                Fx=(G*m1*m2/r**3)*rx
                Fy=(G*m1*m2/r**3)*ry
                Fz=(G*m1*m2/r**3)*rz
                return Vector(Fx,Fy,Fz)

        def GravAcc(self,Force):
                Fx = float(Force.x)
                Fy = float(Force.y)
                Fz = float(FOrce.z)
                mass = float(self.Mass)
                Ax = Fx/mass
                Ay = Fx/mass
                Az = Fx/mass
                return Vector(Ax,Ay,Az)

        def ApplyGravity(self,body):
                ForceVec = self.GravForce(body)
                AccVec = self.GravAcc(ForceVec)
                self.AddAcc(AccVec)

        def ApplyAcc(self):
                '''add acceleration to velocity'''
                self.VelVec.add(self.AccVec)

        def ApplyVel(self):
                '''add velocity to position'''
                self.PosVec.add(self.VelVec)
                










        
