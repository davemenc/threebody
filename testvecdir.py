import math

def VectAngle(x1,y1,z1,x2,y2,z2):
    x1 = float(x1)
    x2 = float(x2)
    y1 = float(y1)
    y2 = float(y2)
    z1 = float(z1)
    z2 = float(z2)
    Theta = math.acos(((x1*x2)+(y1*y2)+(z1*z2))/(math.sqrt(x1**2+y1**2+z1**2)*math.sqrt(x2**2+y2**2+z2**2)))
    
    return Theta
 
def Distance (Pos1,Pos2):
    x1 = float(Pos1[0])
    x2 = float(Pos2[0])
    y1 = float(Pos1[1])
    y2 = float(Pos2[1])
    z1 = float(Pos1[2])
    z2 = float(Pos2[2])
    return math.sqrt((x1-x2)**2+(y1-y2)**2+(z1-z2)**2)
	
def GravForce(Pos1,Pos2,m1,m2):
    x1 = float(Pos1[0])
    x2 = float(Pos2[0])
    y1 = float(Pos1[1])
    y2 = float(Pos2[1])
    z1 = float(Pos1[2])
    z2 = float(Pos2[2])
    m1 = float(m1)
    m2 = float(m2)
    G = 6.67408E-11

    rx=x1-x2
    ry=y1-y2
    rz=z1-z2
    r = Distance(Pos1,Pos2)
##    print "GF r = {x:G}".format(x=r)
##    print "GF Num: {:G}".format(G*m1*m2)
##    print "GF Den: {:G}".format(r**3)
##    print "GF Frac: {:G}".format((G*m1*m2/r**3))
##    print "GF rx: {:G}".format(rx)

    Fx=(G*m1*m2/r**3)*rx
##    print "GF Fx: {:G}".format(Fx)
    Fy=(G*m1*m2/r**3)*ry
    Fz=(G*m1*m2/r**3)*rz
    return [Fx,Fy,Fz]

def GravAcc(Force,mass):
    Fx = float(Force[0])
    Fy = float(Force[1])
    Fz = float(Force[2])
    mass = float(mass)
    Ax = Fx/mass
    Ay = Fx/mass
    Az = Fx/mass
    return [Ax,Ay,Az]
	
	
#Body 1
Pos1 = [0,0,0]
Vel1 = [0,0,0]
M1 = 2.0E30
#Body2
Pos2 = [5.775e9,5.775e9,5.775e9]
Vel2 = [0,0,0]
M2 = 2.0E30
ReportInterval = 10000
RealTimeLimit = 100001

#start position
print "Body 2 position: {x:G},{y:G},{z:G}".format(x=Pos2[0],y=Pos2[1],z=Pos2[2])
#start velocity
print "Body 2 velocity: {x:G},{y:G},{z:G}".format(x=Vel2[0], y=Vel2[1], z=Vel2[2])
#start distance
d = Distance(Pos1,Pos2)
print "Distances: {d:G}".format(d=d)
for t in range(0,RealTimeLimit):
    #force
    f2 = GravForce(Pos1,Pos2,M1,M2)


    #acceleration
    a2 = GravAcc(f2,M2)

    #new velocity
    Vel2[0]+=a2[0]
    Vel2[1]+=a2[1]
    Vel2[2]+=a2[2]

    #new position
    Pos2[0]+=Vel2[0]
    Pos2[1]+=Vel2[1]
    Pos2[2]+=Vel2[2]

    #new distance
    d = Distance(Pos1,Pos2)
    if t%ReportInterval==0:
        print "-----------------------\nTime: {}".format(t)
#        print "Force of gravity: {x:G},{y:G},{z:G}".format(x=f2[0],y=f2[1],z=f2[2])
        print "Body 2 position: {x:G},{y:G},{z:G}".format(x=Pos2[0],y=Pos2[1],z=Pos2[2])
        print "Body 2 velocity: {x:G},{y:G},{z:G}".format(x=Vel2[0], y=Vel2[1], z=Vel2[2])
        print "Body 2 acceleration: {x:G},{y:G},{z:G}".format(x=a2[0], y=a2[1], z=a2[2])
        print "Distances: {d:G}".format(d=d)


print "-----------------------\n-----------------------\nFINAL: {}".format(t)
print "Force of gravity: {x:G},{y:G},{z:G}".format(x=f2[0],y=f2[1],z=f2[2])
print "Body 2 position: {x:G},{y:G},{z:G}".format(x=Pos2[0],y=Pos2[1],z=Pos2[2])
print "Body 2 velocity: {x:G},{y:G},{z:G}".format(x=Vel2[0], y=Vel2[1], z=Vel2[2])
print "Body 2 acceleration: {x:G},{y:G},{z:G}".format(x=a2[0], y=a2[1], z=a2[2])
print "Distances: {d:G}".format(d=d)
