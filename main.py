import math,body,simulation,vector
MAX_ITERATIONS = 1e2 
MAX_REAL_TIME = 5 #seconds
sim = Simulation(time=1,max_duration=MAX_ITERATIONS,max_real_time=MAX_REAL_TIME)
circuit_breaker = 0
while sim.Run():
	sim.Display()
	# limit how long it runs (just in case)
	circuit_breaker+=1
	if circuit_breaker>MAX_ITERATIONS:
		break
sim.Display()